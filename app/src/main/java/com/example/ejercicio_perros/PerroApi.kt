package com.example.ejercicio_perros

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path

interface PerroApi {
    @Headers("Accept: application/json")
    // Método para obtener todos los pokemon

    @GET("breeds/image/random")
    fun getPerro(): Call<Perros>
}
