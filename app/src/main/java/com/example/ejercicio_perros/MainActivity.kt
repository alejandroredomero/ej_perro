package com.example.ejercicio_perros

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.ejercicio_perros.ui.theme.Ejercicio_PerrosTheme
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : ComponentActivity() {
    private lateinit var retrofit: Retrofit
    private lateinit var texto: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        retrofit = retrofit2.Retrofit.Builder()
            .baseUrl("https://dog.ceo/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        texto = obtenerDatos(retrofit)
        setContent {
            Ejercicio_PerrosTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Greeting(texto)
                }
            }
        }
    }
}



private fun obtenerDatos(retrofit : Retrofit) : String {
    var texto = "";
    CoroutineScope(Dispatchers.IO).launch {
        val call =
            retrofit.create(PerroApi::class.java).getPerro().execute()
        val perro = call.body()
        if (call.isSuccessful) {
            texto = perro?.getmessage().toString()
        } else {
            texto = "Ha habido un error"
        }
    }
    Thread.sleep(1000)
    return texto;
}

@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {
    Text(
        text = " $name!",
        modifier = modifier
    )
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    Ejercicio_PerrosTheme {
        Greeting("Android")
    }
}